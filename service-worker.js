const CACHE_NAME = 'whereareuw-v1.1';
const FILES_TO_CACHE = [
  'index.html',
  'offline.html',
  'geo.js',
  'site.css',
  'manifest.json',
  'proj4.js',
];

self.addEventListener('install', function(event){
  console.log('[ServiceWorker] Install');
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page');
      return cache.addAll(FILES_TO_CACHE);
    })
  );

  self.skipWaiting();
});

// clean up old data in cache
self.addEventListener('activate', function(event){
  console.log('[ServiceWorker] Activate')
  event.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key !== CACHE_NAME) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );

  self.clients.claim();
});

self.addEventListener('fetch', (event) => {
  console.log('[ServiceWorker] Fetch', event.request.url);
  if (event.request.mode !== 'navigate') {
    //not a page navigation
    return;
  }
  // page navigation attempted with fetch()
  // if it fails then then it is caught with catch and the offline
  // page is opened from the cache
  event.respondWith(
    caches.match(event.request)
    .then(response => {
      if(response) {
        console.log('Found ', event.request.url, ' in cache');
        return response;
      }
      console.log('Network request for ', event.request.url);
      return fetch(event-request)
    })
    // fetch(event.request)
    //   .catch(() => {
    //     return caches.open(CACHE_NAME)
    //       .then((cache) => {
    //         return cache.match('offline.html');
    //       });
    //   })
  )

});
