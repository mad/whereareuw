window.onload = function(){
  if (navigator.geolocation){
    this.navigator.geolocation.watchPosition(updatePosition)
  }
}

function updatePosition(position){
  let lat = "unknown";
  let long = "unknown";
  let x = "unknown";
  let y = "unknown";

  // Proj4 formatted strings for WGS84 and UTM Zone 17N
  let wgs_projstring = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
  let utm_projstring = "+proj=utm +zone=17 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
  
  // We have both a lat and a long, parse them out into human-friendly strings
  // with a sensible number of decimal places
  if (position.coords.latitude != null && position.coords.longitude != null){
    lat = `${position.coords.latitude.toFixed(4)} \xB0`;
    long = `${position.coords.longitude.toFixed(4)} \xB0`;
    var xy = proj4(wgs_projstring, utm_projstring, [position.coords.longitude, position.coords.latitude]);
    x = `${xy[0].toFixed(4)} m`;
    y = `${xy[1].toFixed(4)} m`;
  }

  let acc = (position.coords.accuracy == null) ? "unknown" : `+/- ${position.coords.accuracy.toFixed(2)} m`;
  let alt = (position.coords.altitude == null) ? "unknown" : `${position.coords.altitude.toFixed(2)} m`;
  let alt_acc = (position.coords.altitudeAccuracy == null) ? "unknown" : `+/- ${position.coords.altitudeAccuracy} m`;
  let heading = (position.coords.heading == null) ? "unknown" : `${position.coords.heading.toFixed(4)} \xB0`;
  let speed = (position.coords.speed == null) ? "unknown" : `${position.coords.speed.toFixed(2)} km/h`;
  document.querySelector("#lat_val").innerText = lat;
  document.querySelector("#long_val").innerText = long;
  document.querySelector("#alt_val").innerText = alt;
  document.querySelector("#x_val").innerText = x;
  document.querySelector("#y_val").innerText = y;
  document.querySelector("#xy_acc_val").innerText = acc;
  document.querySelector("#alt_acc_val").innerText = alt_acc;
  document.querySelector("#head_val").innerText = heading;
  document.querySelector("#speed_val").innerText = speed;
}